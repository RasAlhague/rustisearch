// search.rs search
use crate::search_results::SearchResultItem;
use std::collections::hash_map::HashMap;
pub trait Search {
    fn get_current_directory(&self) -> String;
    fn search(&self, directory: String) -> u32;
    fn is_searching(&self) -> bool;
    fn get_results(&self) -> &Vec<Box<dyn SearchResultItem>>;
    fn add_filter(&mut self, key: String, filter: Box<dyn Fn(&str) -> bool>);
    fn remove_filter(&mut self, key: String);
}
pub struct StandardSearchProvider {
    current_searching_dir: String,
    results: Vec<Box<dyn SearchResultItem>>,
    filter: HashMap<String, Box<dyn Fn(&str) -> bool>>,
    is_searching: bool,
}
impl Search for StandardSearchProvider {
    fn get_current_directory(&self) -> String {
        self.current_searching_dir.clone()
    }

    fn search(&self, directory: String) -> u32 {
        0
    }

    fn is_searching(&self) -> bool {
        false
    }

    fn get_results(&self) -> &Vec<Box<dyn SearchResultItem>> {
        // Maybe hier ne slice zurückgeben
        &self.results
    }

    fn remove_filter(&mut self, key: String) {}

    fn add_filter(&mut self, key: String, filter: Box<dyn Fn(&str) -> bool>) {
        self.filter.insert(key, filter);
    }
}
impl StandardSearchProvider {
    pub fn new() -> StandardSearchProvider {
        let is_searching = false;
        let current_searching_dir = String::new();
        let results: Vec<Box<dyn SearchResultItem>> = vec![];
        let filter: HashMap<String, Box<dyn Fn(&str) -> bool>> = HashMap::new();

        StandardSearchProvider {
            is_searching,
            current_searching_dir,
            results,
            filter,
        }
    }
}
