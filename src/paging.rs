// paging.rs paging
use crate::search_results::SearchResultItem;
use std::convert::TryInto;
pub struct PagedResultList {
    current_page_index: u32,
    page_count: u32,
    page_size: u32,
    results: Vec<Box<dyn SearchResultItem>>,
}
impl PagedResultList {
    pub fn new(results: &Vec<Box<dyn SearchResultItem>>, page_size: u32) -> PagedResultList {
        let current_page_index: u32 = 1;

        let length: u32 = results.len().try_into().unwrap();

        let mut page_count: u32 = length / page_size;

        if length % page_size != 0 {
            page_count += 1;
        }

        let results = results.to_vec();

        PagedResultList {
            page_count,
            current_page_index,
            results,
            page_size,
        }
    }
}
pub trait Paging {
    fn next(&mut self) -> &[Box<dyn SearchResultItem>];
    fn prev(&mut self) -> &[Box<dyn SearchResultItem>];
    fn get_page(&mut self, page_number: u32) -> &[Box<dyn SearchResultItem>];
    fn first(&mut self) -> &[Box<dyn SearchResultItem>];
    fn last(&mut self) -> &[Box<dyn SearchResultItem>];
    fn get_current_page(&self) -> &[Box<dyn SearchResultItem>];
    fn get_current_page_index(&self) -> u32;
    fn get_page_count(&self) -> u32;
    fn get_page_size(&self) -> u32;
    fn change_page_size(&mut self, size: u32) -> &[Box<dyn SearchResultItem>];
}
impl Paging for PagedResultList {
    fn next(&mut self) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }

    fn prev(&mut self) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }

    fn get_page(&mut self, page_number: u32) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }

    fn first(&mut self) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }

    fn last(&mut self) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }

    fn get_current_page(&self) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }

    fn get_current_page_index(&self) -> u32 {
        1
    }

    fn get_page_count(&self) -> u32 {
        1
    }

    fn get_page_size(&self) -> u32 {
        1
    }

    fn change_page_size(&mut self, size: u32) -> &[Box<dyn SearchResultItem>] {
        &self.results[0..1]
    }
}
