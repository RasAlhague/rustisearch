// search_results.rs search_results
pub trait SearchResultItem {
    fn name(&self) -> String;
    fn parent_directory(&self) -> String;
    fn full_name(&self) -> String;
    fn box_clone(&self) -> Box<dyn SearchResultItem>;
    fn open(&self);
}
impl Clone for Box<dyn SearchResultItem> {
    fn clone(&self) -> Box<dyn SearchResultItem> {
        self.box_clone()
    }
}
pub mod file_result {
    use crate::search_results::SearchResultItem;
    #[derive(Clone)]
    pub struct FileResultItem {
        name: String,
        parent_directory: String,
    }
    impl SearchResultItem for FileResultItem {
        fn name(&self) -> String {
            self.name.clone()
        }

        fn parent_directory(&self) -> String {
            self.parent_directory.clone()
        }

        fn full_name(&self) -> String {
            self.parent_directory.clone()
        }

        fn open(&self) {}

        fn box_clone(&self) -> Box<dyn SearchResultItem> {
            Box::new((*self).clone())
        }
    }
}
pub mod directory_result {
    use crate::search_results::SearchResultItem;
    #[derive(Clone)]
    pub struct DirectoryResultItem {
        name: String,
        parent_directory: String,
    }
    impl SearchResultItem for DirectoryResultItem {
        fn name(&self) -> String {
            self.name.clone()
        }

        fn parent_directory(&self) -> String {
            self.parent_directory.clone()
        }

        fn full_name(&self) -> String {
            self.parent_directory.clone()
        }

        fn open(&self) {}

        fn box_clone(&self) -> Box<dyn SearchResultItem> {
            Box::new((*self).clone())
        }
    }
}
