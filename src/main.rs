mod config;
mod paging;
mod search;
mod search_results;
use crate::paging::Paging;
use crate::search::Search;
use std::env;
use std::io;
use std::path::Path;
use std::process;

fn main() {
    println!("Rustisearch V.0.0.1");

    println!("Reading config...");
    let config = config::Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    run(config);
}

fn run(config: config::Config) {
    let mut query = config.query;

    if query == "" {
        println!("Search Query: ");
        print!(">>");
        io::stdin()
            .read_line(&mut query)
            .expect("Failed to read line");
    }

    let mut search_provider = search::StandardSearchProvider::new();
    set_filter(&mut search_provider, query, config.result_type);

    println!("Start searching...");
    let number_of_results: u32 = search_provider.search(config.directory);

    if number_of_results == 0 {
        println!("No result found...");
    } else if number_of_results <= 5 {
        for result in search_provider.get_results().iter() {
            println!("Opening result...");
            result.open();
        }
    } else {
        run_mutliple_result_dialog(&mut search_provider, number_of_results);
    }
}

fn run_mutliple_result_dialog(
    search_provider: &search::StandardSearchProvider,
    number_of_results: u32,
) {
    println!("[2JToo many lines to open!");

    let mut paging_result = paging::PagedResultList::new(search_provider.get_results(), 20);
    let mut command: String = String::new();
    let mut res = paging_result.get_current_page();

    loop {
        println!(
            "Page: {} of {}. Totall results: {}",
            paging_result.get_current_page_index(),
            paging_result.get_page_count(),
            number_of_results
        );
        print_results(&mut paging_result);
        println!("Choose Operation: help = HELP ");
        print!(">>");

        io::stdin()
            .read_line(&mut command)
            .expect("Failed to read line");

        if command.contains("HELP") || command.contains("help") {
            display_help();
        }
        if command.contains("NEXT") || command.contains("next") {
            res = paging_result.next();
        }
        if command.contains("PREV") || command.contains("prev") {
            res = paging_result.prev();
        }
        if command.contains("RANGE") || command.contains("range") {
            let min: u32;
            let max: u32;

            let results = paging_result.get_current_page();
        }
        if command.contains("TAKE") || command.contains("take") {}
        if command.contains("PAGE") || command.contains("page") {
            let mut iter = command.split_whitespace();
            iter.next();

            let page_number: u32 = match iter.next() {
                Some(number) => number.trim().parse().unwrap_or_else(|_err| {
                    eprintln!(
                        "Invalid page for <PAGE> command. Please see help for more information"
                    );
                    0
                }),
                None => 0,
            };

            if page_number != 0 {
                res = paging_result.get_page(page_number);
            }
        }
        if command.contains("EXIT") || command.contains("exit") {
            return;
        }
    }
}

fn display_help() {
    println!("Help for rustisearch:");
    println!("Commands:");
    println!("[HELP | help] = Displays the command help");
    println!("[NEXT | next] = Displays the next {} results", 20);
    println!("[PREV | prev] = Displayes the prevouis {} results", 20);
    println!("[FIRST | first] = Displays the first {} results", 20);
    println!("[LAST | last] = Displayes the last {} results", 20);
    println!("[RANGE | range] = Opens the results in the range. Sample: RANGE 0-30");
    println!("[TAKE | take] = Opens the results which are separated by \",\". Sample: TAKE 1,4,6");
    println!("[PAGE | page] = Displayes the results from page X. Sample: PAGE 5 ");
    println!("[EXIT | exit] = Closes the programm");
}

fn print_results(paging_result: &paging::PagedResultList) {}

fn set_filter(
    search_provider: &mut search::StandardSearchProvider,
    query: String,
    result_type: String,
) {
    search_provider.add_filter(
        String::from("query_filter"),
        Box::new(move |val| val.contains(&query)),
    );

    if result_type == "file" {
        search_provider.add_filter(
            String::from("file_filter"),
            Box::new(|val| Path::new(val).is_dir()),
        );
    }
    if result_type == "dir" {
        search_provider.add_filter(
            String::from("directory_filter"),
            Box::new(|val| Path::new(val).is_file()),
        );
    }
}
