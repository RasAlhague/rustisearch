// config.rs config
pub struct Config {
    pub query: String,
    pub directory: String,
    pub result_type: String,
}
impl Config {
    pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> {
        args.next();

        let directory = match args.next() {
            Some(arg) => arg,
            None => return Err("Did not get a directory!"),
        };

        let result_type = match args.next() {
            Some(arg) => arg,
            None => return Err("Did not get a result type!"),
        };

        let query = match args.next() {
            Some(arg) => arg,
            None => String::new(),
        };

        Ok(Config {
            query,
            directory,
            result_type,
        })
    }
}
